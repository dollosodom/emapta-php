<?php

require "vendor/autoload.php";

use Emapta\Parsers\CsvParser;
use Emapta\Operation\CashOperation\CashOperation;
use Emapta\Operation\CashOperation\CashOperationService;
use Emapta\Commission\CommissionService;

// Parse given data
$csvParser = new CsvParser($argv[1]);
$parsedValues = $csvParser->parse();

// Create Cash Operation objects from parsed data

$cashOperations = [];
foreach ($parsedValues as $parsedValue) {
    $cashOperationService = new CashOperationService();
    $cashOperation = $cashOperationService->mapCsvDataToCashOperation($parsedValue);
    array_push($cashOperations, $cashOperation);
}

// Create Commission objects then compute commission amount from Cash Operations
foreach ($cashOperations as $cashOperation) {
    $commissionService = new CommissionService();
    $commission = $commissionService->computeCommission($cashOperation);
    print_r($commission->amount . "\n");
}


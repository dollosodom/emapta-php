<?php

namespace Emapta\User;

/**
 * 
 */
abstract class UserType
{
    const LEGAL = "legal";
    const NATURAL = "natural";
}
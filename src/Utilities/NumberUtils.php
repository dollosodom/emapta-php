<?php

namespace Emapta\Utilities;
/**
 * 
 */
class NumberUtils
{
    public static function toNumberFormat($number, $precision = 2) {
        return number_format((float)$number, $precision, '.', '');
    }

    public static function ceilingRoundUp($number, $precision = 2) {
        $decimalDigitCount = strlen(substr(strrchr($number, "."), 1));
        if ($decimalDigitCount > $precision) {
            return ceil($number * pow(10, $precision)) / pow(10, $precision);
        } else {
            return self::toNumberFormat($number);
        }
    }

    public static function toPercentage($number) {
        return $number / 100;
    }
}
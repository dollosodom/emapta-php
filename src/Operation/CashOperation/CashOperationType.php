<?php

namespace Emapta\Operation\CashOperation;

/**
 * 
 */
abstract class CashOperationType
{   
    const CASH_IN = "cash_in";
    const CASH_OUT = "cash_out";
}
<?php

namespace Emapta\Operation\CashOperation;

/**
 * 
 */
class CashOperationService implements CashOperationServiceInterface
{
    
    public function mapCsvDataToCashOperation($parsedValue) {
        $cashOperation = new CashOperation();
        $cashOperation->userId = $parsedValue[1];
        $cashOperation->userType = $parsedValue[2];
        $cashOperation->type = $parsedValue[3];
        $cashOperation->amount = $parsedValue[4];
        $cashOperation->currency = $parsedValue[5];
        $cashOperation->date = $parsedValue[0];

        return $cashOperation;
    }
}
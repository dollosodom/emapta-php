<?php

namespace Emapta\Operation\CashOperation;

interface CashOperationServiceInterface {
    
    public function mapCsvDataToCashOperation($parsedValues);
}
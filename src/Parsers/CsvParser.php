<?php

namespace Emapta\Parsers;

/**
 * 
 */
class CsvParser extends Parser implements ParserInterface
{
    
    function __construct($fileName)
    {
        parent::__construct($fileName);
        $this->validateFileExtension();
    }

    public function parse() {
        $dataCollection = [];
        $file = fopen($this->fileName, "r");
        if ($file) {
            while (($data = fgetcsv($file, 1000)) !== false) {
                array_push($dataCollection, $data);
            }
        }
        fclose($file);
        return $dataCollection;
    }

    protected function validateFileExtension() {
        $fileParts = pathinfo($this->fileName);
        if ($fileParts["extension"] !== "csv") {
            echo "invalid filename extension";
        }
    }
}
<?php

namespace Emapta\Parsers;

interface ParserInterface {
	public function parse();
}


<?php

namespace Emapta\Parsers;

/**
 * 
 */
abstract class Parser
{
	protected $fileName;
	
	function __construct($fileName)
	{
		$this->fileName = $fileName;
	}

	abstract protected function validateFileExtension();
}
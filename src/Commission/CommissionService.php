<?php

namespace Emapta\Commission;

use Emapta\Operation\CashOperation\CashOperation;
use Emapta\Operation\CashOperation\CashOperationType;
use Emapta\Utilities\NumberUtils;
use Emapta\User\UserType;

/**
 * 
 */
class CommissionService implements CommissionServiceInterface
{
    
    public function computeCommission(CashOperation $cashOperation) {
        if ($cashOperation->type === CashOperationType::CASH_IN) {
            return $this->computeCashIn($cashOperation);
        } else if ($cashOperation->type === CashOperationType::CASH_OUT) {
            return $this->computeCashOut($cashOperation);
        } else {

        }
    }

    private function computeCashIn(CashOperation $cashOperation) {
        $computedCommission = $cashOperation->amount * NumberUtils::toPercentage(0.03);

        $commission = new Commission();

        if ($computedCommission > 5) {
            $commission->amount = NumberUtils::toNumberFormat(5);
        } else {
            $commission->amount = NumberUtils::toNumberFormat(NumberUtils::ceilingRoundUp($computedCommission));
        }

        return $commission;
    }

    private function computeCashOut(CashOperation $cashOperation) {
        $computedCommission = $cashOperation->amount * NumberUtils::toPercentage(0.3);
        $commission = new Commission();

        if ($cashOperation->userType === UserType::NATURAL) {
            $commission->amount = NumberUtils::toNumberFormat(NumberUtils::ceilingRoundUp($computedCommission));
        } else if ($cashOperation->userType === UserType::LEGAL) {
            if ($computedCommission < 0.5) {
                $commission->amount = NumberUtils::toNumberFormat(0.5);
            } else {
                $commission->amount = NumberUtils::toNumberFormat(NumberUtils::ceilingRoundUp($computedCommission));
            }
        }

        return $commission;
    }
}
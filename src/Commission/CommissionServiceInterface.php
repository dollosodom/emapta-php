<?php

namespace Emapta\Commission;

use Emapta\Operation\CashOperation\CashOperation;

interface CommissionServiceInterface {

    public function computeCommission(CashOperation $operation);

}
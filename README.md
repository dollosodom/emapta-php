# emapta-php

## Install dependencies

```
php composer.phar install
php composer.phar dumpautoload -o
```

## Usage

```
php index.php input.csv
```

## Tests

```
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/Commission/CommissionServiceTest.php
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/Operation/CashOperation/CashOperationServiceTest.php
./vendor/bin/phpunit --bootstrap vendor/autoload.php tests/Utilities/NumberUtilsTest.php
```
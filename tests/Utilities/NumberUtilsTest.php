<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Emapta\Utilities\NumberUtils;

/**
 * 
 */
class NumberUtilsTest extends TestCase
{
    public function testToNumberFormat(): void {
        $this->assertEquals(1000.00, NumberUtils::toNumberFormat(1000));

        $this->assertEquals(1000.0000, NumberUtils::toNumberFormat(1000, 4));
    }

    public function testCeilingRoundUp(): void {
        $this->assertEquals(1000.23, NumberUtils::ceilingRoundUp(1000.2222));

        $this->assertEquals(1000.28, NumberUtils::ceilingRoundUp(1000.28));
    }
    
    public function testToPercentage(): void {
        $this->assertEquals(0.5, NumberUtils::toPercentage(50));
    }
}
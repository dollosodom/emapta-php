<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Emapta\Operation\CashOperation\CashOperation;
use Emapta\Operation\CashOperation\CashOperationService;

/**
 * 
 */
class CashOperationServiceTest extends TestCase
{
    public function testMapCsvDataToCashOperation(): void {

        $parsedValue = ["2014-12-31", 4 ,"natural", "cash_out", 1200.00, "EUR"];

        $cashOperationService = new CashOperationService();
        $cashOperation = $cashOperationService->mapCsvDataToCashOperation($parsedValue);

        $this->assertInstanceOf(CashOperation::class, $cashOperation);
    }
}
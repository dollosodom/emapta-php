<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Emapta\Utilities\NumberUtils;
use Emapta\Operation\CashOperation\CashOperation;
use Emapta\Commission\CommissionService;
use Emapta\Commission\Commission;

/**
 * 
 */
class NumberUtilsTest extends TestCase
{

    public function testComputeCommission() {

        /* Test cash in that will not exceed cap commission */

        $cashOperation = new CashOperation();
        $cashOperation->userId = 4;
        $cashOperation->userType = "natural";
        $cashOperation->type = "cash_in";
        $cashOperation->amount = 200.00;
        $cashOperation->currency = "EUR";
        $cashOperation->date = "2016-01-05";

        $commissionService = new CommissionService();
        $commission = $commissionService->computeCommission($cashOperation);

        $this->assertInstanceOf(Commission::class, $commission);
        $this->assertEquals($commission->amount, 0.06);


        /* Test cash in that will exceed cap commission */

        $cashOperation = new CashOperation();
        $cashOperation->userId = 4;
        $cashOperation->userType = "natural";
        $cashOperation->type = "cash_in";
        $cashOperation->amount = 120000.00;
        $cashOperation->currency = "EUR";
        $cashOperation->date = "2014-12-31";

        $commissionService = new CommissionService();
        $commission = $commissionService->computeCommission($cashOperation);

        $this->assertInstanceOf(Commission::class, $commission);
        $this->assertEquals($commission->amount, 5.00);


        /* Test cash out performed by a natural person */

        $cashOperation = new CashOperation();
        $cashOperation->userId = 4;
        $cashOperation->userType = "natural";
        $cashOperation->type = "cash_out";
        $cashOperation->amount = 1200.00;
        $cashOperation->currency = "EUR";
        $cashOperation->date = "2014-12-31";

        $commissionService = new CommissionService();
        $commission = $commissionService->computeCommission($cashOperation);

        $this->assertInstanceOf(Commission::class, $commission);
        $this->assertEquals($commission->amount, 3.60);


        /* Test cash out performed by a legal person */

        $cashOperation = new CashOperation();
        $cashOperation->userId = 4;
        $cashOperation->userType = "legal";
        $cashOperation->type = "cash_out";
        $cashOperation->amount = 1200.00;
        $cashOperation->currency = "EUR";
        $cashOperation->date = "2014-12-31";

        $commissionService = new CommissionService();
        $commission = $commissionService->computeCommission($cashOperation);

        $this->assertInstanceOf(Commission::class, $commission);
        $this->assertEquals($commission->amount, 3.6);

        /* Test cash out performed by a legal person and below minimum commission */

        $cashOperation = new CashOperation();
        $cashOperation->userId = 4;
        $cashOperation->userType = "legal";
        $cashOperation->type = "cash_out";
        $cashOperation->amount = 10.00;
        $cashOperation->currency = "EUR";
        $cashOperation->date = "2014-12-31";

        $commissionService = new CommissionService();
        $commission = $commissionService->computeCommission($cashOperation);

        $this->assertInstanceOf(Commission::class, $commission);
        $this->assertEquals($commission->amount, 0.5);
    }
}